# Benchme

<b>Victor Minon </b><br>
<b>Sahan Kananke Acharige </b><br>
3ICS

<i>Projet d'algorithme de tri en C</i>
 ***

<br>

## Projet

Ce projet consiste à tester différents algorithmes de tri sur des tableaux de tailles différentes et de générer un fichier CSV contenant les temps moyens de traitement pour chaque algorithme sur chaque tableau.
<br>
Moyenne effectuée avec 3 valeurs.


***

### Les 4 algorithmes de tri sont les suivants : 

- #### 1)  ***Le tri à bulle***

    Cette méthode permet aux valeurs les plus petites de se déplacer vers le haut du tableau et de laisser les valeurs les plus hautes descendre.  
    Ainsi une paire d'éléments sont comparées et sont permutées si nécessaire, si les deux élements sont les même ou dans le bon ordre le tout reste intact.

- ####  2) ***Le tri par séléction***

    Dans cette méthode l'algorithme va chercher l'élément le plus petit et va le ranger directement au bon endroit.
    Ainsi une comparaison va être effectuée en un élément et tous les suivants pour trouver le premier élément plus petit.

- ####  3) ***Le tri par insertion*** 

    Cette méthode est la plus longue. En effet l'algorithme va traiter chaque élément un par un et les positionner en fonction des précédents donc avec un grand tableau cela peut prendre du temps.

- ####  4) ***Le tri par tas***

    L'algorithme du tri par tas repose sur un élément fondamental : le tas (d'où son nom). En effet, ce tri crée un tas max du tableau donné en entrée, et le parcourt afin de reconstituer les valeurs triées dans notre tableau.
    L'algorithme va alors analyser les éléments un par un sous forme de "tas" et va enlever le plus grand à chaque fois.

<br>

## Usage des commandes 
- make all : lance le programme<br>
- make documentation : affiche la documentation<br>
- make clean : nettoie les repertoires contenant le code sources

<br>

## Résultats attendus
- Le programme doit être capable de créer des tableaux contenant jusqu'a 10⁷ valeurs aléatoires. 
- Le programme doit pouvoir trier ces tableaux avec différentes méthodes  et retourner un fichier "benchmark" au format CSV contenant les temps moyens de tri pour chaque méthode.
- Tout cela doit se faire de manière automatique sans l'intervention de l'utilisateur. 
  
<br>

## Evolution a venir 
- Implémentation de nouvelles methodes de tri
- Tableaux de taille plus grandes
- Tri de tableaux avec des valeurs précises et non aléatoire
