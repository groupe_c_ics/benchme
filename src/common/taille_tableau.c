/**
 * @file taille_tableau.c
 * @author Victor Minon & Sahan Kananke Acharige
 * @version 1.0
 * @brief calcul taille tableau
 * @date 06-10-2021
 */

#include <stdio.h>
#include "../../include/function.h"

int taille_tableau(float a[])
{
    int taille = 0;
    while(a[taille] != '\0')
    {
        taille++;
    }
    return taille-1;
}

int taille_tableau_int(int a[])
{
    int taille = 0;
    while(a[taille] != '\0')
    {
        taille++;
    }
    return taille-1;
}