/**
 * @file randoam_float.c
 * @author Victor Minon & Sahan Kananke Acharige
 * @version 1.0
 * @brief génération aléatoire de float pour remplir les tableau
 * @date 06-10-2021
 */
#include <time.h>
#include <stdlib.h>
#include "../../include/function.h"

float float_rand()
{

    float num = (((float)rand() / (float)(RAND_MAX)) * 1000000000.0);
    return num;
}
