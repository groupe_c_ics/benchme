/**
 * @file ecrire_fichier.c
 * @author Victor Minon & Sahan Kananke Acharige
 * @version 1.0
 * @brief création du fichier "benchme_result.csv"
 * @date 06-10-2021
 */

#include <stdlib.h>
#include <stdio.h>
#include "../../include/function.h"

void creer_fichier()
{
    FILE* fichier = NULL;

    fichier = fopen("benchme_result.csv", "w");
    fprintf(fichier,"nombre,type,temps\n");
    fclose(fichier);
}

void ecrire_fichier(float temps, char * typetri, int nb_taille)
{
    FILE* fichier = NULL;

    fichier = fopen("benchme_result.csv", "a");
    fprintf(fichier,"%d,%s,%f\n", nb_taille, typetri, temps);
    fclose(fichier);
}