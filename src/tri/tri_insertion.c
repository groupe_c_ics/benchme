/**
 * @file tri_insertion.c
 * @author Victor Minon & Sahan Kananke Acharige
 * @version 1.0
 * @brief algorithme de tri par insertion
 * @date 06-10-2021
 */

#include <stdio.h>
#include <stdlib.h>
#include "../../include/function.h"

float *tri_insertion(float tableau[],int taille)
{
    int i, compt, marqueur;
    float temp;

    for(i=1;i<taille;i++)
    {
        temp=tableau[i];
        compt=i-1;

        do
        {
            marqueur=0;
            if (tableau[compt]>temp)
            {
                tableau[compt+1]=tableau[compt];
                compt--;
                marqueur=1;
            }
            if (compt<0) marqueur=0;
        }
        while(marqueur);
        tableau[compt+1]=temp;
    }
    return tableau;

}

float *tri_insertion_dec(float tableau[],int taille)
{
    int i, compt, marqueur;
    float temp;

    for(i=1;i<taille;i++)
    {
        temp=tableau[i];
        compt=i-1;

        do
        {
            marqueur=0;
            if (tableau[compt]<temp)
            {
                tableau[compt+1]=tableau[compt];
                compt--;
                marqueur=1;
            }
            if (compt<0)
            {
                marqueur=0;
            }
        }
        while(marqueur);
        {
        tableau[compt+1]=temp;
        }
    }
    return tableau;

}