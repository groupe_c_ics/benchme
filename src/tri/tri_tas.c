/**
 * @file tri_tas.c
 * @author Victor Minon & Sahan Kananke Acharige
 * @version 1.0
 * @brief algorithme de tri par tas
 * @date 06-10-2021
 */
#include <stdio.h>
#include "../../include/function.h"

void tamiser(float * tableau, int noeud, int n)
{
    int j = 2*noeud;
    while (j <= n)
    {
        if ((j < n) && (tableau[j] < tableau[j+1]))
        {
            j++;
        }
        else if (tableau[noeud] < tableau[j])
        {
            float temp = tableau[noeud];
            tableau[noeud] = tableau[j];
            tableau[j] = temp;
            noeud = j;
            j = 2*noeud;
        }
        else {
            j = n+1;
        }
    }
}

float * tri_tas(float tableau[],int taille)
{
    for (int i = taille/2 - 1; i >= 0; i--)
    {
        tamiser(tableau, i, taille);
    }
    for (int i = taille - 1; i >= 0 ; i--)
    {
        float temp = tableau[0];
        tableau[0] = tableau[i];
        tableau[i] = temp;
        tamiser(tableau, 0, i-1);
    }
}

void tamiser_dec(float * tableau, int noeud, int n)
{
    int j = 2*noeud;
    while (j <= n)
    {
        if ((j > n) && (tableau[j] > tableau[j+1]))
        {
            j++;
        }
        else if (tableau[noeud] > tableau[j])
        {
            float temp = tableau[noeud];
            tableau[noeud] = tableau[j];
            tableau[j] = temp;
            noeud = j;
            j = 2*noeud;
        }
        else {
            j = n+1;
        }
    }
}

float * tri_tas_dec(float tableau[],int taille)
{
    for (int i = taille/2 - 1; i >= 0; i--)
    {
        tamiser_dec(tableau, i, taille);
    }
    for (int i = taille - 1; i >= 0 ; i--)
    {
        float temp = tableau[0];
        tableau[0] = tableau[i];
        tableau[i] = temp;
        tamiser(tableau, 0, i-1);
    }
}

