/**
 * @file tri_bulle.c
 * @author Victor Minon & Sahan Kananke Acharige
 * @version 1.0
 * @brief algorithme de tri a bulle
 * @date 06-10-2021
 */

#include <stdio.h>
#include "../../include/function.h"

float * tri_bulle(float tableau[],int taille)
{
    int i, j, tmp;

    for (i=0 ; i < taille-1; i++)
    {
        for (j=0 ; j < taille-i-1; j++)
        {
            if (tableau[j] > tableau[j+1])
            {
                tmp = tableau[j];
                tableau[j] = tableau[j+1];
                tableau[j+1] = tmp;
            }
        }
    }
    return tableau;
}

float * tri_bulle_dec(float tableau[],int taille)
{
    int i, j, tmp;

    for (i=0 ; i < taille-1; i++)
    {
        for (j=0 ; j < taille-i-1; j++)
        {
            if (tableau[j] > tableau[j+1])
            {
                tmp = tableau[j];
                tableau[j] = tableau[j+1];
                tableau[j+1] = tmp;
            }
        }
    }
    return tableau;
}