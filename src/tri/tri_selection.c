/**
 * @file tri_selection.c
 * @author Victor Minon & Sahan Kananke Acharige
 * @version 1.0
 * @brief algorithme de tri par selection
 * @date 06-10-2021
 */

#include <stdio.h>
#include "../../include/function.h"

float * tri_selection(float tableau[],int taille)
{
    int i, j, index;
    float tmp;

    for (i=0; i < (taille-1); i++)
    {
        index = i;

        for (j=i + 1; j < taille; j++)
        {
            if (tableau[index] > tableau[j])
                index = j;
        }
        if (index != i)
        {
            tmp = tableau[i];
            tableau[i] = tableau[index];
            tableau[index] = tmp;
        }
    }
    return tableau;
}

float * tri_selection_dec(float tableau[],int taille)
{
    int i, j, index;
    float tmp;

    for (i=0; i < (taille-1); i++)
    {
        index = i;

        for (j=i + 1; j < taille; j++)
        {
            if (tableau[index] < tableau[j])
                index = j;
        }
        if (index != i)
        {
            tmp = tableau[i];
            tableau[i] = tableau[index];
            tableau[index] = tmp;
        }
    }
    return tableau;
}