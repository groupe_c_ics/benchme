/**
 * @file main.c
 * @author Victor Minon & Sahan Kananke Acharige
 * @version 1.5
 * @brief fichier main executant les 4 algorithmes de tri pour tous les tableau 
 * @date 06-10-2021
 */

/*! \mainpage Accueil
 *
 *
 * Benchme est une application de benchmarks de fonctions de 
 * tri avec génération des résultats dans un fichier CSV : "benchme_result.csv"
 * \n
 * 
 * \subsection step1 Algorithme de tri :
 *   -tri a bulle
 * \n-tri par selection
 * \n-tri par insertion
 * \n-tri par tas
 * \n
 * 
 * \subsection step2 Commande :
 *    make all : execute le programme
 * \n make documentation : affiche cette page
 * \n make clean : nettoie les repertoires contenant le code sources
 */


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "../include/function.h"


int main()
{
    creer_fichier();


    clock_t debut, fin;
    float temps;
    int taille[6] = {100,1000,10000,100000,1000000,10000000,};
    int taille_modele = taille_tableau_int(taille);


    for(int j=0; j < taille_modele; j++)
    {
        float moy_insertion=0, moy_bulle=0, moy_selection=0, moy_tas = 0;
        for(int y = 0; y < 3; y++){
            srand(time(0));
            float *tableau;
            tableau = malloc(taille[j]*sizeof(float));
            for(int i=0; i < taille[j]; i++)
            {
                tableau[i] = float_rand();
            }

            //tri par insertion
            debut = clock();
            float *insertion = NULL;
            insertion = malloc(taille[j]*sizeof(float));
            insertion = tri_insertion(tableau, taille[j]);
            fin = clock();
            temps = (float) (fin - debut)/CLOCKS_PER_SEC;

            moy_insertion += temps;



            //tri par bulle
            debut = clock();
            float *bulle = NULL;
            bulle = malloc(taille[j]*sizeof(float));
            bulle = tri_bulle(tableau, taille[j]);
            fin = clock();
            temps = (float) (fin - debut)/CLOCKS_PER_SEC;

            moy_bulle += temps;


            //tri par selection
            debut = clock();
            float *selection = NULL;
            selection = malloc(taille[j]*sizeof(float));
            selection = tri_selection(tableau, taille[j]);
            fin = clock();
            temps = (float) (fin - debut)/CLOCKS_PER_SEC;

            moy_selection += temps;


            //tri par tas
            debut = clock();
            float *tas = NULL;
            tas = malloc(taille[j]*sizeof(float));
            tas = tri_tas(tableau, taille[j]);
            fin = clock();
            temps = (float) (fin - debut)/CLOCKS_PER_SEC;

            moy_tas += temps;
        }
        ecrire_fichier(moy_insertion/3, "insertion", taille[j]);
        ecrire_fichier(moy_bulle/3, "bulle", taille[j]);
        ecrire_fichier(moy_selection/3, "selection", taille[j]);
        ecrire_fichier(moy_tas/3, "tas", taille[j]);
    }

    return 0;
}

